#include "stdio.h"
#include "matr.h"

int main(int argc, char **argv) {
    int x[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int y[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    pretty_print_matrix(stdout, x, 3, 3);
    printf("X\n");
    pretty_print_matrix(stdout, y, 3, 3);
    printf("=\n");
    int *res = mul_matrix(x, 3, 3, y, 3, 3);
    pretty_print_matrix(stdout, res, 3, 3);

    int x1[16] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
    int y1[4] = {1, 0, 0, 1};

    pretty_print_matrix(stdout, x1, 4, 4);
    printf("X\n");
    pretty_print_matrix(stdout, y1, 2, 2);
    printf("=\n");
    res = block_right_mul_matrix(x1, 4, 4, y1, 2, 2);
    pretty_print_matrix(stdout, res, 4, 4);

    pretty_print_matrix(stdout, y1, 2, 2);
    printf("X\n");
    pretty_print_matrix(stdout, x1, 4, 4);
    printf("=\n");
    res = block_left_mul_matrix(y1, 2, 2, x1, 4, 4);
    pretty_print_matrix(stdout, res, 4, 4);

    pretty_print_matrix(stdout, x1, 4, 4);
    printf("transposed is\n");
    res = transpose(x1, 4, 4);
    pretty_print_matrix(stdout, res, 4, 4);

    return 0;
}