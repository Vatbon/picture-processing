//
// Created by vatbon on 20.06.2020.
//
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "omp.h"

void copy_block(const int *from, int w1, int h1, int *in, int w2, int h2);

void paste_block(const int *from, int w1, int h1, int *in, int w2, int h2);

void ssum_matrix(int *x, int *y, int w, int h);

int *mul_matrix_on_scalar(int *x, int w, int h, int scalar);

int *transpose(const int *x, int w, int h);

int *mul_matrix_transpose(int *x, int w1, int h1, int *y, int w2, int h2)
{
    if (w1 != h2)
        return NULL;

    int *res = (int *)malloc(sizeof(int) * h1 * w2);
    int sum;
    int i, j, k;
    int *_y = transpose(y, w2, h2);
#pragma omp parallel private(i, j, k, sum)
    {
        for (i = 0; i < h1; ++i)
        {
            for (j = 0; j < w2; ++j)
            {
                sum = 0;
                for (k = 0; k < w1; ++k)
                {
                    sum += x[w1 * i + k] * _y[h2 * j + k];
                }
                res[w2 * i + j] = sum;
            }
        }
    }
    free(_y);
    return res;
}

void pretty_print_matrix(FILE *output, int *matrix, int w, int h)
{
    if (matrix != NULL)
    {
        int max_len = 1;
        int t;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                t = (int)log10(abs(matrix[w * j + i])) + 1;
                if (matrix[w * j + i] < 0)
                    t++;
                max_len = max_len < t ? t : max_len;
            }
        }
        char *str = malloc(4 + max_len + 1);
        sprintf(str, "%%-%dd ", max_len);
        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                fprintf(output, str, matrix[w * j + i]);
            }
            fprintf(output, "\n");
        }
    }
}

int *mul_matrix(int *x, int w1, int h1, int *y, int w2, int h2)
{
    if (w1 != h2)
        return NULL;
    int *res;
    int sum;
    int i, j, k;
    if (w2 >= 256)
    {
        res = mul_matrix_transpose(x, w1, h1, y, w2, h2);
    }
    else
    {
        res = (int *)malloc(sizeof(int) * h1 * w2);
#pragma omp parallel private(i, j, k, sum)
        {
            for (i = 0; i < h1; ++i)
            {
                for (j = 0; j < w2; ++j)
                {
                    sum = 0;
                    for (k = 0; k < w1; ++k)
                    {
                        sum += x[w1 * i + k] * y[w2 * k + j];
                    }
                    res[w2 * i + j] = sum;
                }
            }
        }
    }
    return res;
}

int *sum_matrix(const int *x, const int *y, int w, int h)
{
    int *out = (int *)malloc(w * h * sizeof(int));
    for (int i = 0; i < h * w; ++i)
        out[i] = x[i] + y[i];
}

int *block_left_mul_matrix(const int *x, int w1, int h1, const int *y, int w2, int h2)
{
    int block_w = w2 / w1;
    int block_h = h2 / h1;
    int *res = (int *)malloc(sizeof(int) * w2 * h2);
    int i, j, k;
    int *blocks = (int *)malloc(sizeof(int) * w2 * h2);

    for (j = 0; j < w1; ++j)
    {
        for (k = 0; k < h1; ++k)
        {
            copy_block(y + block_w * j + block_h * w2 * k, w2, h2,
                       blocks + k * block_h * block_w + block_h * block_w * w1 * j, block_w, block_h);
        }
    }
#pragma omp parallel private(i, j, k)
    {
        int *buffer = (int *)malloc(sizeof(int) * block_h * block_w);
        int *tt;
        for (i = 0; i < h1; ++i)
        {
            for (j = 0; j < w1; ++j)
            {
                for (int i1 = 0; i1 < block_h * block_w; ++i1)
                {
                    buffer[i1] = 0;
                }

                for (k = 0; k < h1; ++k)
                {
                    tt = mul_matrix_on_scalar(blocks + k * block_h * block_w + block_h * block_w * w1 * j, block_w, block_h, x[w1 * i + k]);
                    ssum_matrix(buffer, tt, block_w, block_h);
                    free(tt);
                }
                paste_block(buffer, block_w, block_h,
                            res + block_w * j + block_h * w2 * i, w2, h2);
            }
        }
        free(buffer);
    }
    free(blocks);
    return res;
}

int *block_right_mul_matrix(const int *x, int w1, int h1, const int *y, int w2, int h2)
{
    int block_w = w1 / w2;
    int block_h = h1 / h2;
    int *res = (int *)malloc(sizeof(int) * w1 * h1);
    int i, j, k;
    int *blocks = (int *)malloc(sizeof(int) * w1 * h1);
    for (j = 0; j < h2; ++j)
    {
        for (k = 0; k < w2; ++k)
        {
            copy_block(x + block_w * k + block_h * w1 * j, w1, h1,
                       blocks + k * block_h * block_w + block_h * block_w * w2 * j, block_w, block_h);
        }
    }
#pragma omp parallel private(i, j, k)
    {
        int *buffer = (int *)malloc(sizeof(int) * block_h * block_w);
        int *tt;
        for (i = 0; i < h2; ++i)
        {
            for (j = 0; j < w2; ++j)
            {
                for (int i1 = 0; i1 < block_h * block_w; ++i1)
                {
                    buffer[i1] = 0;
                }

                for (k = 0; k < h2; ++k)
                {
                    tt = mul_matrix_on_scalar(blocks + k * block_h * block_w + block_h * block_w * w2 * i, block_w, block_h, y[w2 * k + j]);
                    ssum_matrix(buffer, tt, block_w, block_h);
                    free(tt);
                }
                paste_block(buffer, block_w, block_h,
                            res + block_w * j + block_h * w1 * i, w1, h1);
            }
        }
        free(buffer);
    }
    free(blocks);
    return res;
}

int *transpose(const int *x, int w, int h)
{
    int *res = (int *)malloc(w * h * sizeof(int));
    for (int i = 0; i < w; ++i)
        for (int j = 0; j < h; ++j)
            res[h * i + j] = x[w * j + i];
    return res;
}

void paste_block(const int *from, int w1, int h1, int *in, int w2, int h2)
{
    int i, j;
    for (i = 0; i < h1; ++i)
        for (j = 0; j < w1; ++j)
            in[w2 * i + j] = from[w1 * i + j];
}

void copy_block(const int *from, int w1, int h1, int *in, int w2, int h2)
{
    int i, j;
    for (i = 0; i < h2; ++i)
        for (j = 0; j < w2; ++j)
            in[w2 * i + j] = from[w1 * i + j];
}

void ssum_matrix(int *x, int *y, int w, int h)
{
    for (int i = 0; i < h * w; ++i)
        x[i] += y[i];
}

int *mul_matrix_on_scalar(int *x, int w, int h, int scalar)
{
    int *res = (int *)malloc(sizeof(int) * w * h);
    for (int i = 0; i < w * h; ++i)
        res[i] = x[i] * scalar;
    return res;
}
