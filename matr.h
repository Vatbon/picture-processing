//
// Created by vatbon on 20.06.2020.
//

#include "stdio.h"

#ifndef PICTURE_PROCESSING_MATR_H
#define PICTURE_PROCESSING_MATR_H

void pretty_print_matrix(FILE *output, int *matrix, int w, int h);

/* Allocates memory for result matrix and returns result
 * of matrices multiplication result.
 *
 * NULL, if it's impossible to calculate*/
int *mul_matrix(int *x, int w1, int h1, int *y, int w2, int h2);

/* Allocates memory for result matrix and returns result
 * of matrices sum result.
 *
 * NULL, if it's impossible to calculate*/
int *sum_matrix(const int *x, const int *y, int w, int h);

/* Allocates memory for result matrix and returns result
 * of matrices right block multiplication result.
 *
 * y is block matrix
 * NULL, if it's impossible to calculate*/
int *block_right_mul_matrix(const int *x, int w1, int h1, const int *y, int w2, int h2);

/* Allocates memory for result matrix and returns result
 * of matrices left block multiplication result.
 *
 * x is block matrix
 * NULL, if it's impossible to calculate*/
int *block_left_mul_matrix(const int *x, int w1, int h1, const int *y, int w2, int h2);

/* Allocates memory for result matrix and returns transposed matrix*/
int *transpose(const int *x, int w, int h);

#endif //PICTURE_PROCESSING_MATR_H
